package com.main;

import com.model.Student;
import com.service.StudentService;
import com.service.StudentServiceImpl;

public class MainApp {

	public static void main(String[] args) {
		
		Student student1 = new Student("Sony", 10, 98f);
		Student student2 = new Student("BPL", 20, 88f);
		Student student3 = new Student("Samsung", 30, 78f);
		Student student4 = new Student("Redmi", 40, 94f);
		
		Student[] students = new Student[4];
		students[0] = student1;
		students[1] = student2;
		students[2] = student3;
		students[3] = student4;
		
		StudentService studentService = new StudentServiceImpl();
		Student obj = studentService.getStudentById(students, 40);
		if(obj!= null) {
			System.out.println("Student id : "+obj.getStudentId());
			System.out.println("Student name : "+obj.getStudentName());
		}
		else {
			System.out.println("No such Student");
		}
		
		
	}

}
