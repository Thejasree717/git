package com.service;

import com.model.Student;

public class StudentServiceImpl implements StudentService {

	@Override
	public Student getStudentByName(Student[] students, String name) {
		Student student = null;
		if (students != null && students.length>0 && name!= null && name.isEmpty()) {
			for(int i=0; i<students.length;i++) {
				if(students[i].getStudentName().equalsIgnoreCase(name)) {
					student = students[i];
				}
			}
			
		} else {
			//Exception
		}
		return student;
	}

	@Override
	public Student getStudentById(Student[] students, int studentId) {
		Student student = null;
		if (students != null && students.length>0 && studentId>0) {
			for(int i=0; i<students.length;i++) {
				if(students[i].getStudentId() == studentId) {
					student = students[i];
				}
			}
			
		} else {
			//Exception
		}
		return student;
	}

}
